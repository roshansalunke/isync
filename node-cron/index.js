const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const cron = require('node-cron');
const moment = require('moment');


app.use('*', cors());
app.use(bodyParser.json());

app.get('/', (req, res)=>{
    res.send("API is up and running");
})

app.listen(9090, ()=>{
    console.log('API is up and running');
})

// cron.schedule('* * * * *', ()=>{
//     console.log('running the task every minute', moment().format('DD MMM YYYY hh:mm:ss'));
// })

// cron.schedule('* * * * * *', ()=>{
//     console.log('running the task every second', moment().format('DD MMM YYYY hh:mm:ss'));
// })

// set value
// cron.schedule('*/10 * * * *', ()=>{
//     console.log('running the task every 10 sec', moment().format('DD MMM YYYY hh:mm:ss'));
// })

//  range
// cron.schedule('5-10 * * * * *', ()=>{
//     console.log('running the task between 5-10 sec', moment().format('DD MMM YYYY hh:mm:ss'));
// })

// multiple value
// cron.schedule('5,15,25,35 * * * * *', ()=>{
//     console.log('running the task every 10 sec', moment().format('DD MMM YYYY hh:mm:ss'));
// })

const task = cron.schedule('* * * * * *', ()=>{
    console.log('running the task every sec', moment().format('DD MMM YYYY hh:mm:ss'));
},{
    scheduled : false
})

task.start();
// task.stop();

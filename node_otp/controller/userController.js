require('dotenv').config();
// const messagebird = require('messagebird').initClient(process.env.MSSG_KEY);
var nodemailer = require('nodemailer');

// exports.userLogin = (req, res) => {
//     const { phonenumber } = req.body;
//     const newPhoneNumber = "+91"+phonenumber;

//     var params = {
//         template: 'Your Login OTP is %token',
//         timeout: 300
//     };

//     messagebird.verify.create(newPhoneNumber, params, (err, response) => {
//         if (err) {
//             console.log("OTP Send Error", err);
//             res.status(200).send({ "status": "failed", "message": "Unable to Send OTP"})

//         }
//         console.log("OTP Send Response: ",response);
//         // res.status(200).send({ "status": "success", "message": "OTP Send Successfully", "id": response.id})
//     });
// }

exports.userMail = (req, res) => {

    const your_mail = req.body.your_mail;
    const to = req.body.to;
    const password = req.body.password;
    console.log(password);

    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: your_mail,
            pass: password
        }
    });

    var mailOptions = {
        from: your_mail,
        to: to,
        subject: 'Sending Email using Node.js',
        text: 'That was easy!'
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
            res.send({ "Email sent:": info.response})
        }
    });
}
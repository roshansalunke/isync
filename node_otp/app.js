const express = require('express');
var nodemailer = require('nodemailer');

const Handlebars = require('handlebars')
var { engine } = require('express-handlebars');
// var userRoutes = require('./routes/user')
const bodyParser = require('body-parser');
require('dotenv').config();
const app = express();
const port = process. env.PORT || 8000;
//Parsing midddleware
app.use(bodyParser.urlencoded({ extended: false }));

//Parse application/json

app.use(bodyParser.json());

app.set('view engine', 'hbs');


// app.use("/api/user",userRoutes);

const routes = require('./routes/user');
app.use('/', routes);


app.listen(port, () => console.log(`Listening on port ${port}`));

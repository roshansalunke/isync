const express = require('express');
const Handlebars = require('handlebars')
var { engine } = require('express-handlebars');
const path = require("path");
const { allowInsecurePrototypeAccess } = require('@handlebars/allow-prototype-access')
const bodyParser = require('body-parser');
const mongodb = require("mongodb");
const mongoose = require("mongoose");
require('dotenv').config();
const app = express();
const port = process.env.PORT || 4000;
// const CircularJSON = require('circular-json');

// const StudentsModel = require('./app/model/StudentsModel');

//Parsing midddleware
app.use(bodyParser.urlencoded({ extended: false }));

//Parse application/json

app.use(bodyParser.json());

app.use(express.static('public'))

// Templating Engine
app.set("views", path.join(__dirname, "app/views"));

app.engine('hbs', engine({ extname: '.hbs', handlebars: allowInsecurePrototypeAccess(Handlebars), helpers: require("./app/helper/LogicHelper"),}));
app.set('view engine', 'hbs');


const routes = require('./app/routes/user');
app.use('/', routes);


app.listen(port, () => console.log(`Listening on port ${port}`));

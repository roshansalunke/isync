const mongodb = require("mongodb");
const mongoose = require("mongoose");
const StudentsModel = require('../model/StudentsModel');
const CountryModel = require('../model/CountryModel');


// db connection
mongoose.connect(process.env.DB_CONNECTION, {useNewUrlParser:true, useUnifiedTopology:true})
.then( ()=> {

   console.log('DB Connected!');
})
.catch( (err) => {
  console.log(err);
});


//view User
exports.view = async (req, res) => {
    const data = await StudentsModel.find().populate("country_id", "name")
    // const data = await StudentsModel.find().populate({path : "country_id", select : "name", match: {name : "USA" }}).sort({_id: 'desc'})

    // .then(data=>res.render('../views/home', {data}))
    // .catch(error=>console.log(error));
    console.log(data);
    res.render('../views/home', {data});
}
//find user
exports.find = async (req, res) => {
    let searchTearm = new RegExp(req.body.search, "i");
    // const data = await StudentsModel.find({$text: {$search: searchTearm }});
    const data = await StudentsModel.find({$or:[{name : searchTearm}, {city: searchTearm}]});
    // console.log(searchTearm.includes(data));


   res.render('../views/home', {data});
}


//add User
exports.add = async (req, res) => {
    const country = await CountryModel.find();
    // console.log(country);
    res.render('add', {country});
    
}

//add User action
exports.add_action = async (req, res) => {
    
        let name = req.body.name;
        let age = req.body.age;
        let city = req.body.city;
        let country_id =  req.body.country
        // console.log(country_id);
        await StudentsModel.insertMany({name: name, age: age, city: city, country_id: country_id});
        // await StudentsModel.insertMany({name: name, age: age, city: city});
        res.redirect('/');
}



exports.delete = async (req, res) => {

    let id = req.params.id;
    await StudentsModel.deleteOne({_id : id});
    res.redirect('/');
}



exports.edit = async (req, res) => {
        let id = req.params.id;
        const country = await CountryModel.find();
        
        const data = await StudentsModel.findOne({_id: id});
        // const data = await StudentsModel.findOne({_id: id}).populate("country_id")
        console.log(country);

        res.render('edit', { data : data, country : country});
}

//edit User
exports.edit_action = async (req, res) => {
    let id = req.body.id;
    let name = req.body.name;
    let age = req.body.age;
    let city = req.body.city;
    var myquery = { _id: id };
    var newvalues = { $set: {name: name, age: age, city: city } };
    await StudentsModel.updateOne(myquery, newvalues);        
    res.redirect('/');
        
}

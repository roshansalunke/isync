const { Handlebars } = require("express-handlebars");
// const req = require("express/lib/request");
// const async = require("hbs/lib/async");

module.exports = {
    if_equal: function (a, b, options) {
        if (a == b) {
            return options.fn(this);
        }
        return options.inverse(this);
    },
    isSelected: function (value, key) {
        console.log(key);
        // console.log(key);
        
        return value == key ? "selected" : "";
    },
};

const mongoose = require("mongoose");

const StudentsSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    age: {
        type: Number,
        required: true,
    },
    city: {
        type: String,
        required: true,
    },
    country_id: {
        type: String,
        required: true,
        ref: "country",
    },
});



module.exports = mongoose.model("student", StudentsSchema);;
const UsersModel = require('../models/userModel');
require('../helpers/init_mongodb')
const createError = require('http-errors')
const {authSchema} = require('../helpers/validation_schema')
const {signAccessToken} = require('../helpers/jwt_helper')


exports.view = async (req, res) => {
    res.send('Hello from express');
}

exports.reg_action = async (req, res, next) => {
    // console.log(req.body);
    try{
/*         const email = req.body.email;
        const password = req.body.password; */

        const result = await authSchema.validateAsync(req.body)

        // console.log(result);

        // if(!email || !password) throw UsersModel.createError.BadRequest()
    
        const doesExist = await UsersModel.findOne({email: result.email})
        if(doesExist) throw createError.Conflict(`${result.email} is already been register`)
    
        const user = new UsersModel(result)
        const saveUser = await user.save()
        const accessToken = await signAccessToken(saveUser.id)
        res.render('login')
        
    }catch(error){
        if (error.isJoi == true) error.status = 422
        next(error)
    }
    // res.send('Hello from registration');
}

exports.login = async (req, res) => {

    res.render("login");

}

exports.reg = async (req, res) => {

    res.render("registration");

}
exports.login_action = async (req, res, next) => {

    try{
        const result = await authSchema.validateAsync(req.body)
        const user = await UsersModel.findOne({email: result.email})
        
        if(!user) throw createError.NotFound("User Not register")

        const isMatch = await user.isValidPassword(result.password)
        console.log(isMatch)
        // if(!isMatch) throw createError.Unauthorized("Username/password not valid")
        var a ='';
        // if(!isMatch){
        //     var a = 1
        //     res.render('login', {a})
        // }

        const accessToken = await signAccessToken(user.id)
        if(!isMatch){
            // var a = 1
            // res.render('login', {a})
            req.flash('error', 'Invalid username or password!');
            res.redirect('/login')

        }else{

            req.session.accesstoken = accessToken
            // console.log(req.session.accesstoken)
            res.redirect('/dashboard')
        }

    }catch (error){
        const a = error.isJoi
        console.log(a);
        // if (error.isJoi == true) return next(createError.BadRequest("Inavlid Username/Password"))
        if (error.isJoi == true){
            res.render('login', {error})
        }
        //  return next(createError.BadRequest("Inavlid Username/Password"))

        next(error)
    }

}

exports.refresh_token = async (req, res) => {
    res.send('Hello from refresh_token');
}

exports.logout = async (req, res) => {
    res.send('Hello from logout');
}
exports.dashboard = async (req, res) => {
    res.render('dashboard');
}

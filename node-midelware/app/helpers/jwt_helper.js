const { required } = require('@hapi/joi')
const jwt = require('jsonwebtoken')
const createError = require('http-errors')
const { token } = require('morgan')
require('dotenv').config();



module.exports = {
    signAccessToken: (usserID) => {

        return new Promise((resolve, reject) => {
            const payload = {}
            const secret = process.env.ACCESS_TOKEN_SECRET
            const option = {
                expiresIn: "1h",
                issuer: "pickurpage.com",
                audience: usserID
            }
            jwt.sign(payload, secret, option, (err, token) => {
                if(err) {
                    console.log(err.message)
                    reject(createError.InternalServerError())
                }
                resolve(token)
            })
        })
    },
    verifyAccessToken: (req, res, next) => {
        const accessToken = req.session.accesstoken
        // if (!accessToken) return next(createError.Unauthorized())
        if (!accessToken){
            res.redirect('/login');
        }
        // const authHeader =req.session.accesstoken['authorization']
        // const bearerToken = authHeader.split(' ')
        const token = accessToken;
        jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, payload) =>{

            if(err){
                return next(createError.Unauthorized())
            }
            req.payload = payload
            next()
        })

    }
}
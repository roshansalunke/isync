const mongodb = require("mongodb");
const mongoose = require("mongoose");
require('dotenv').config();

// db connection
mongoose.connect(process.env.DB_CONNECTION, {useNewUrlParser:true, useUnifiedTopology:true})
.then( ()=> {

   console.log('DB Connected!');
})
.catch( (err) => {
  console.log(err);
});
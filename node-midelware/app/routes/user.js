const express = require('express');
const router = express.Router();
const userController = require('../controller/userController');
const {verifyAccessToken} = require('../helpers/jwt_helper')


//create, find, update, delete
router.get('/',  userController.view);
// router.get('/', verifyAccessToken, userController.view);
router.get('/registre',userController.reg);
router.post('/registration_action',userController.reg_action);
router.get('/login',userController.login);
router.get('/dashboard',verifyAccessToken, userController.dashboard);
router.post('/login_action',userController.login_action);
// router.post('/refresh-token',userController.refresh_token);
// router.delete('/logout',userController.logout);

module.exports = router;
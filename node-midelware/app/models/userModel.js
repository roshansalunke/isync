// const { error } = require("@hapi/joi/lib/base");
const mongoose = require("mongoose");
const bcrypt = require('bcrypt')


const UsersSchema = mongoose.Schema({
    email: {
        type: String,
        required: true,
        lowercase: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
    }
});


UsersSchema.pre('save', async function(next){
    try{
        const salt = await bcrypt.genSalt(10)

        const hashpassword = await bcrypt.hash(this.password, salt)
        this.password = hashpassword
        next()
    }catch(error){
        next(error)
    }
})


UsersSchema.methods.isValidPassword = async function(password){
    try{

        return await bcrypt.compare(password, this.password)

    }catch (error){

        throw error 
    }
}

module.exports = mongoose.model("user", UsersSchema);;

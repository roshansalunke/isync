const express = require('express');
const Handlebars = require('handlebars')
const path = require("path");
const session = require("express-session");
var { engine } = require('express-handlebars');
const morgan = require("morgan");
const flash = require('express-flash');
const createError = require("http-errors");
// const { allowInsecurePrototypeAccess } = require('@handlebars/allow-prototype-access')
const bodyParser = require('body-parser');
const fs = require('fs');


// require('./app/helpers/init_mongodb')

require('dotenv').config();
const app = express();
app.use(flash());

const port = process.env.PORT;
// const CircularJSON = require('circular-json');
app.use(
    session({
        secret: process.env.SESSION_SECRET,
        resave: false,
        saveUninitialized: false,
    })
);
app.use(async function (req, res, next) {
    res.locals.session = req.session;
    res.locals.name = "Roshan";
    res.locals.sessionFlash = req.flash();
    console.log(req.flash())
    next();
});


// const StudentsModel = require('./app/model/StudentsModel');

app.use(morgan('dev'));

//Parsing midddleware
app.use(bodyParser.urlencoded({ extended: false }));

//Parse application/json

app.use(bodyParser.json());

app.use(express.static('public'))

// Templating Engine
app.set("views", path.join(__dirname, "app/views"));

app.engine('hbs', engine({ extname: '.hbs' }));
app.set('view engine', 'hbs');

app.use(express.static(__dirname + "/public/assets"));
const routes = require('./app/routes/user');
app.use('/', routes);

app.use(async(req, res, next)=>{
    // const error = new Error("Not Found");
    // error.status = 404;
    // next(error);

    next(createError.NotFound())

})

app.use((err, req, res, next)=>{

    res.status(err.status || 500)
    res.send({
        error:{
            status:err.status || 500,
            message: err.message,
        },
    })
})

app.listen(port, () => console.log(`Listening on port ${port}`));

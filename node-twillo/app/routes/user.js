const express = require('express');
const router = express.Router();
const userController = require('../controller/userController');
// const {verifyAccessToken} = require('../helpers/jwt_helper')


//create, find, update, delete
router.get('/',  userController.view);  
router.get('/login',  userController.login);  
router.post('/login_action',  userController.login_action);  
router.get('/verify',  userController.verify);  
router.post('/verify_action',  userController.verify_action);  
router.get('/welcome',  userController.welcome);  

module.exports = router;
require('dotenv').config();
const mongodb = require("mongodb");
const mongoose = require("mongoose");
const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
const client = require('twilio')(accountSid, authToken);

const UserModel = require('../model/UserModel');

// db connection
mongoose.connect(process.env.DB_CONNECTION, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {

        console.log('DB Connected!');
    })
    .catch((err) => {
        console.log(err);
    });



exports.view = async (req, res) => {
    res.send('Hello from express');
    // res.render('login')
}


exports.login = async (req, res) => {
    // res.send('Hello from express');
    res.render('login')
}

exports.login_action = async (req, res) => {
    const number = req.body.number;

    let randomNo = Math.floor(Math.random() * 90000) + 10000;

    let regex = new RegExp(/[6-9][0-9]{9}/);
    if (number == null) {
        return "false";
    }

    // Return true if the mobile_number
    // matched the ReGex
    if (regex.test(number) != true) {
        console.log("InCorrect")
        req.flash('error', 'Invalid Phone Number!');
        res.redirect('/login')
    }
    else {
        client.messages
            .create({
                body: randomNo,
                from: '+16073182674',
                to: "+91" + number
            })
            .then(saveUser());

        function saveUser() {
            var newUser = new UserModel({ user: randomNo });
            newUser.save()
            res.redirect('/verify')
           
        }

    }
}

exports.verify = async (req, res) => {
    // res.send('Hello from express');
    res.render('verify')
}

exports.verify_action = async (req, res) => {
    // res.send('Hello from express');
    // res.render('verify')
    const code = req.body.otp
    // const user = await UserModel.findOne({user:code})
    UserModel.findOneAndDelete({ user: code }).then(function (result) {
        if (result == null) {
            console.log("Wrong OTP")
            req.flash('error', 'Invalid  OTP');
            res.redirect('/verify')
        } else {
            console.log("Deleted")
            res.redirect('/welcome')
        }
    })
}
exports.welcome = async (req, res) => {
    // res.send('Hello from express');
    res.render('welcome')
}
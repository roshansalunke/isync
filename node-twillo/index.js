const express = require('express');
const Handlebars = require('handlebars')
const path = require("path");
const session = require("express-session");

var { engine } = require('express-handlebars');
const flash = require('express-flash');
const bodyParser = require('body-parser');
require('dotenv').config();
const app = express();
app.use(flash());

app.use(bodyParser.urlencoded({ extended: false }));

//Parse application/json

app.use(
    session({
        secret: process.env.SESSION_SECRET,
        resave: false,
        saveUninitialized: false,
    })
);
app.use(async function (req, res, next) {
    res.locals.session = req.session;
    res.locals.name = "Roshan";
    res.locals.sessionFlash = req.flash();
    // console.log(req.flash())
    next();
});

app.use(bodyParser.json());
app.use(express.static('public'))

const port = process.env.PORT;

app.set("views", path.join(__dirname, "app/views"));

app.engine('hbs', engine({extname: '.hbs'}));
app.set('view engine', 'hbs');

app.use(express.static(__dirname + "/public/assets"));
const routes = require('./app/routes/user');
app.use('/', routes);



app.listen(port, () => console.log(`Listening on port ${port}`));

const express = require("express");
const path = require("path");
const cors = require("cors");
const bodyParser = require("body-parser");
const mongodb = require("mongodb");
const mongoose = require("mongoose");
const { engine } = require("express-handlebars");
const session = require("express-session");
const fileupload = require("express-fileupload");
const app = express();
const port = process.env.PORT || 9000;
const CompanyDetailModel = require("./app/model/CompanyDetailModel");
const UserModel = require("./app/model/UserModel");
require("dotenv/config");

const LoginRoute = require("./app/routes/LoginRoute");
const DashboardRoute = require("./app/routes/DashboardRoute");
const CategoryRoute = require("./app/routes/CategoryRoute");
const UserRoute = require("./app/routes/UserRoute");
const ProductRoute = require("./app/routes/ProductRoute");
const ProfileRoute = require("./app/routes/ProfileRoute");
const CompanyDetailRoute = require("./app/routes/CompanyDetailRoute");
const EmailSettingRoute = require("./app/routes/EmailSettingRoute");
const SocialSettingRoute = require("./app/routes/SocialSettingRoute");
const OfferRoute = require("./app/routes/OfferRoute");
const SystemEmailRoute = require("./app/routes/SystemEmailRoute");
const LanguageLabel = require("./app/routes/LanguageLabel");
const async = require("hbs/lib/async");
const flash = require("express-flash");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileupload());
app.use(cors());
app.use(flash());
app.use(
    session({
        secret: process.env.SESSION_SECRET,
        resave: false,
        saveUninitialized: false,
    })
);
app.use(async function (req, res, next) {
    res.locals.session = req.session;
    res.locals.sessionFlash = req.flash();
    next();
});

mongoose.connect(
    process.env.DB_CONNECTION, { useNewUrlParser: true, useUnifiedTopology: true },
    () => console.log("Connected")
);

app.set("port", process.env.PORT || 9000);
app.set("views", path.join(__dirname, "app/view"));
app.engine(
    "handlebars",
    engine({
        defaultLayout: "index",
        runtimeOptions: {
            allowProtoPropertiesByDefault: true,
            allowProtoMethodsByDefault: true
        },
        partialsDir: __dirname + "/app/view/layouts",
        helpers: require("./app/helper/LogicHelper"),
        library: require('./app/library/GeneralLibrary'),
    })
);
app.set("view engine", "handlebars");
app.use(express.static(__dirname + "/public/assets"));

app.use("/", LoginRoute);
app.use("/login", LoginRoute);

app.use(async function (req, res, next) {
    if (req.session.email) {
        try {
            var SQL = {};
            await CompanyDetailModel.findOne(SQL).lean().exec().then((companydata) => {
                res.locals.companyData = companydata;
            });
            await UserModel.findOne(SQL).lean().exec().then((userdata) => {
                res.locals.userData = userdata;
            });
            var current_route = req.originalUrl.replace(/\?.*$/, '').split("/");
            res.locals.route = current_route[1];
            next();
        } catch (error) {
            console.log(error);
        }
    } else {
        res.locals.companyData = null;
        res.locals.userData = null;
        next();
    }
});

app.use("/dashboard", DashboardRoute);
app.use("/category", CategoryRoute);
app.use("/user", UserRoute);
app.use("/product", ProductRoute);
app.use("/profile", ProfileRoute);
app.use("/company-detail", CompanyDetailRoute);
app.use("/email-setting", EmailSettingRoute);
app.use("/social-setting", SocialSettingRoute);
app.use("/offer", OfferRoute);
app.use("/system-email", SystemEmailRoute);
app.use("/language-label", LanguageLabel);

app.get("/", function (req, res) {
    if (req.session.email) {
        res.redirect("/dashboard");
    } else {
        res.redirect("/");
    }
});

app.listen(port, "0.0.0.0", () => {
    console.log("Server is running on port: " + port);
});
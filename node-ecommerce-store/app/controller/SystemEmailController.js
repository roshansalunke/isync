const async = require("hbs/lib/async");
const SystemEmailModel = require("../model/SystemEmailModel");
const Paginator = require("../library/PaginatorLibrary");

exports.index = async(req, res) => {

    if (req.session.email) {
        is_layout = true;
        res.render("../view/systemEmail/index", {
            is_layout: is_layout,
        });
    } else {
        res.redirect("/");
    }

};

exports.ajax_listing = async(req, res) => {
    var vEmailCode = req.body.vEmailCode;
    var iSystemEmailId = req.body.iSystemEmailId;
    var eStatus = req.body.eStatus;
    var vAction = req.body.vAction;
    var vPage = req.body.vPage;

    if (vAction === "delete" && iSystemEmailId != null) {
        await SystemEmailModel.deleteOne({ _id: iSystemEmailId });
    }

    if (vAction === "search") {
        var SQL = {};
        if (vEmailCode.length > 0) {
            var vEmailCodeSearch = new RegExp(vEmailCode, "i");
            SQL.vEmailCode = vEmailCodeSearch;
        }
        if (eStatus.length > 0) {
            var eStatusSearch = eStatus;
            SQL.eStatus = eStatusSearch;
        }

        try {
            var dataCount = await SystemEmailModel.count(SQL);

            // Pagination
            let vPage = 1;
            let vItemPerPage = 10;
            let vCount = dataCount;

            if (req.body.vPage != "") {
                vPage = req.body.vPage;
            }

            let criteria = {
                vPage: vPage,
                vItemPerPage: vItemPerPage,
                vCount: vCount
            }

            let paginator = Paginator.pagination(criteria);

            let start = (vPage - 1) * vItemPerPage;
            let limit = vItemPerPage;
            // End

            var systemEmailData = await SystemEmailModel.find(SQL).skip(start).limit(limit);

            res.render("../view/systemEmail/ajax_listing", {
                layout: false,
                systemEmailData: systemEmailData,
                paginator: paginator
            });
        } catch (error) {
            res.render("../view/systemEmail/ajax_listing", {
                layout: false,
                systemEmailData: error
            });
        }
    } else {
        try {
            var systemEmailData = await SystemEmailModel.find();
            res.render("../view/systemEmail/ajax_listing", {
                layout: false,
                systemEmailData: systemEmailData
            });
        } catch (error) {
            res.render("../view/systemEmail/ajax_listing", {
                layout: false,
                systemEmailData: error
            });
        }
    }
}

exports.add = async(req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/systemEmail/add", {
                is_layout: is_layout,
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async(req, res) => {

    if (req.session.email) {

        if (req.body.iSystemEmailId) {
            try {
                SystemEmailModel.findOneAndUpdate({ _id: req.body.iSystemEmailId }, {
                    "$set": {
                        "vEmailCode": req.body.vEmailCode,
                        "vEmailTitle": req.body.vEmailTitle,
                        "vFromName": req.body.vFromName,
                        "vFromEmail": req.body.vFromEmail,
                        "vCcEmail": req.body.vCcEmail,
                        "vBccEmail": req.body.vBccEmail,
                        "vEmailSubject": req.body.vEmailSubject,
                        "tEmailMessage": req.body.tEmailMessage,
                        "tSmsMessage": req.body.tSmsMessage,
                        "tInternalMessage": req.body.tInternalMessage,
                        "eStatus": req.body.eStatus,
                    }
                }).exec(function(error, result) {
                    if (!error) {
                        res.redirect("/system-email");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            const SystemEmail = new SystemEmailModel({
                vEmailCode: req.body.vEmailCode,
                vEmailTitle: req.body.vEmailTitle,
                vFromName: req.body.vFromName,
                vFromEmail: req.body.vFromEmail,
                vCcEmail: req.body.vCcEmail,
                vBccEmail: req.body.vBccEmail,
                vEmailSubject: req.body.vEmailSubject,
                tEmailMessage: req.body.tEmailMessage,
                tSmsMessage: req.body.tSmsMessage,
                tInternalMessage: req.body.tInternalMessage,
                eStatus: req.body.eStatus,
            });
            try {
                SystemEmail.save(function(error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("/system-email");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }

    }
};

exports.edit = async(req, res) => {

    var iSystemEmailId = req.params.iSystemEmailId;

    if (req.session.email) {
        try {
            var systemEmailData = await SystemEmailModel.findOne({ _id: iSystemEmailId });
            is_layout = true;
            res.render("../view/systemEmail/add", {
                is_layout: is_layout,
                systemEmailData: systemEmailData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};
const async = require("hbs/lib/async");
const UserModel = require("../model/UserModel");
const ProductModel = require("../model/ProductModel");
const CategoryModel = require("../model/CategoryModel");
const OfferModel = require('../model/OfferModel');

exports.index = async (req, res) => {

    if (req.session.email) {
        var SQL = {};
        SQL.eStatus = "Active";
        var total_user = await UserModel.count(SQL).exec();
        var total_category = await CategoryModel.count(SQL).exec();
        var total_product = await ProductModel.count(SQL).exec();
        var total_offer = await OfferModel.count(SQL).exec();

        var profile = await UserModel.findOne({ _id: req.session.userid });

        var categoryData = await CategoryModel.find(SQL).limit(5);
        var productData = await ProductModel.find(SQL).limit(5).populate({ path: 'iCategoryId', select: 'vCategory' });

        is_layout = true;
        res.render("../view/dashboard/index", {
            is_layout: is_layout,
            total_category: total_category,
            total_user: total_user,
            total_offer: total_offer,
            total_product: total_product,
            categoryData: categoryData,
            productData: productData,
            profile: profile,
        });
    } else {
        res.redirect("/");
    }

};
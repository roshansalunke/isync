const async = require("hbs/lib/async");
const LanguageLabelModel = require("../model/LanguageLabelModel");
const Paginator = require("../library/PaginatorLibrary");
const fs = require('fs');
const path = require('path');

exports.index = async (req, res) => {

    if (req.session.email) {
        is_layout = true;
        res.render("../view/language_label/index", {
            is_layout: is_layout,
        });
    } else {
        res.redirect("/");
    }

};

exports.ajax_listing = async (req, res) => {
    var vLabel = req.body.vLabel;
    var eStatus = req.body.eStatus;
    var vTitle = req.body.vTitle;
    var iLanguageLabelId = req.body.iLanguageLabelId;
    var vAction = req.body.vAction;
    var vPage = req.body.vPage;

    if (vAction === "delete" && iLanguageLabelId != null) {
        await LanguageLabelModel.deleteOne({ _id: iLanguageLabelId });
    }

    if (vAction === "multiple_delete" && iLanguageLabelId != null) {
        let languageLabelID = iLanguageLabelId.split(',');

        await LanguageLabelModel.deleteMany({ _id: { $in: languageLabelID } });
    }

    if (vAction === "search") {
        var SQL = {};
        if (vLabel.length > 0) {
            var vLabelSearch = new RegExp(vLabel, "i");
            SQL.vLabel = vLabelSearch;
        }
        if (eStatus.length > 0) {
            var eStatusSearch = eStatus;
            SQL.eStatus = eStatusSearch;
        }
        if (vTitle.length > 0) {
            var vTitleSearch = vTitle;
            SQL.vTitle = vTitleSearch;
        }

        try {
            // Pagination
            var dataCount = await LanguageLabelModel.count(SQL);
            let vPage = 1;
            let vItemPerPage = 10;
            let vCount = dataCount;

            if (req.body.vPage != "") {
                vPage = req.body.vPage;
            }

            let criteria = {
                vPage: vPage,
                vItemPerPage: vItemPerPage,
                vCount: vCount
            }

            let paginator = Paginator.pagination(criteria);

            let start = (vPage - 1) * vItemPerPage;
            let limit = vItemPerPage;
            // End

            var languageLabelData = await LanguageLabelModel.find(SQL).skip(start).limit(limit);

            res.render("../view/language_label/ajax_listing", {
                layout: false,
                languageLabelData: languageLabelData,
                paginator: paginator
            });
        } catch (error) {
            res.render("../view/language_label/ajax_listing", {
                layout: false,
                languageLabelData: error
            });
        }
    } else {
        try {
            // Pagination
            var dataCount = await LanguageLabelModel.count(SQL);
            let vPage = 1;
            let vItemPerPage = 10;
            let vCount = dataCount;

            if (req.body.vPage != "") {
                vPage = req.body.vPage;
            }

            let criteria = {
                vPage: vPage,
                vItemPerPage: vItemPerPage,
                vCount: vCount
            }

            let paginator = Paginator.pagination(criteria);

            let start = (vPage - 1) * vItemPerPage;
            let limit = vItemPerPage;
            // End

            var languageLabelData = await LanguageLabelModel.find(SQL).skip(start).limit(limit);

            res.render("../view/language_label/ajax_listing", {
                layout: false,
                languageLabelData: languageLabelData,
                paginator: paginator
            });
        } catch (error) {
            res.render("../view/language_label/ajax_listing", {
                layout: false,
                languageLabelData: error
            });
        }
    }
}

exports.add = async (req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/language_label/add", {
                is_layout: is_layout,
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {

    if (req.session.email) {

        if (req.body.iLanguageLabelId) {
            try {
                LanguageLabelModel.findOneAndUpdate({ _id: req.body.iLanguageLabelId }, { "$set": { "vLabel": req.body.vLabel, "eStatus": req.body.eStatus, "vTitle": req.body.vTitle } }).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/language-label");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            const LanguageLabel = new LanguageLabelModel({
                vLabel: req.body.vLabel,
                eStatus: req.body.eStatus,
                vTitle: req.body.vTitle,
                iOrder: req.body.iOrder,
            });
            try {
                LanguageLabel.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("/language-label");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }

    }
};

exports.edit = async (req, res) => {

    var iLanguageLabelId = req.params.iLanguageLabelId;

    if (req.session.email) {
        try {
            var languageLabelData = await LanguageLabelModel.findOne({ _id: iLanguageLabelId });
            is_layout = true;
            res.render("../view/language_label/add", {
                is_layout: is_layout,
                languageLabelData: languageLabelData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};

exports.generate = async (req, res) => {

    if (req.session.email) {
        try {
            var languageLabelData = await LanguageLabelModel.find();
            var langCount = languageLabelData.length;
            var langCountTotal = langCount - 1;
            var line = '';
            line += `[` + "\n";
            for (let index = 0; index < languageLabelData.length; index++) {
                if (langCountTotal == index) {
                    var comma = '';
                } else {
                    var comma = ',';
                }
                const element = languageLabelData[index];
                line += "\t" + "{" + "\n" + "\t\t" + '"' + element.vLabel + '"' + ":" + '"' + element.vTitle + '"' + "\n" + "\t" + "}" + comma + "\n";
            }
            line += `]`;

            fs.writeFile("/var/www/html/projects/nodejs/node-ecommerce-store/app/lang/en/message.json", line, err => {
                if (err) {
                    console.error(err);
                } else {
                    console.log("file write done");
                }
            });
            res.redirect("/language-label");
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};
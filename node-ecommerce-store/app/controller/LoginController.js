const bcrypt = require("bcrypt");
const UserModel = require("../model/UserModel");

exports.index = (req, res) => {
    if (req.session.email) {
        res.redirect("/dashboard");
    } else {
        is_layout = false;
        res.render("../view/login/index", {
            is_layout: is_layout
        });
    }
};

exports.login_action = async (req, res) => {
    const vEmail = req.body.vEmail;
    const vPassword = req.body.vPassword;
    var vError = false;

    if (vEmail.lenght === 0) {
        vError = true;
    }
    if (vPassword.lenght === 0) {
        vError = true;
    }
    if (vError === false) {

        try {
            const userData = await UserModel.findOne({
                vEmail: vEmail,
                eType: "Admin"
            });

            if (userData == null) {
                req.session.message = '';
                req.flash('error', 'Email Account Not Found');
                res.redirect("/");
            } else {
                // if (await bcrypt.compare(vPassword, userData.vPassword)) {

                req.session.firstname = userData.vFirstName;
                req.session.lastname = userData.vLastName;
                req.session.email = userData.vEmail;
                req.session.userid = userData._id;

                req.flash('success', 'Login Successfully');
                res.redirect("/dashboard");

                // } else {
                //     req.flash('error', 'Invalid Password');
                //     res.redirect("/");
                // }
            }
        } catch (error) {

            req.session.message = error;
            res.redirect("/");

        }

    } else {
        req.flash('error', 'Email or Password Empty');
        res.redirect("/");
    }

};

exports.logout = async (req, res) => {
    req.session.destroy();
    res.redirect("/dashboard");
};
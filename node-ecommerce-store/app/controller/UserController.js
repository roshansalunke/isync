const UserModel = require("../model/UserModel");
const EmailLibrary = require('../library/EmailLibrary');
const SystemEmailModel = require("../model/SystemEmailModel");
const bcrypt = require("bcrypt");

exports.index = async (req, res) => {

    if (req.session.email) {
        is_layout = true;
        res.render("../view/user/index", {
            is_layout: is_layout,
        });
    } else {
        res.redirect("/");
    }

};

exports.ajax_user = async (req, res) => {
    var vFirstName = req.body.vFirstName;
    var vEmail = req.body.vEmail;
    var eType = req.body.eType;
    var eStatus = req.body.eStatus;
    var iUserId = req.body.iUserId;
    var vAction = req.body.vAction;
    var vPage = req.body.vPage;

    if (vAction === "delete" && iUserId != null) {
        await UserModel.deleteOne({ _id: iUserId });
    }

    if (vAction === "search") {
        var SQL = {};
        if (vFirstName.length > 0) {
            var vFirstNameSearch = new RegExp(vFirstName, "i");
            SQL.vFirstName = vFirstNameSearch;
        }
        if (vEmail.length > 0) {
            var vEmailSearch = new RegExp(vEmail, "i");
            SQL.vEmail = vEmailSearch;
        }
        if (eStatus.length > 0) {
            var eStatusSearch = eStatus;
            SQL.eStatus = eStatusSearch;
        }
        if (eType.length > 0) {
            var eTypeSearch = eType;
            SQL.eType = eTypeSearch;
        }

        try {
            var userData = await UserModel.find(SQL);
            res.render("../view/user/ajax_user", {
                layout: false,
                userData: userData
            });
        } catch (error) {
            res.render("../view/user/ajax_user", {
                layout: false,
                userData: error
            });
        }
    } else {
        try {
            var userData = await UserModel.find();
            res.render("../view/user/ajax_user", {
                layout: false,
                userData: userData
            });
        } catch (error) {
            res.render("../view/user/ajax_user", {
                layout: false,
                userData: error
            });
        }
    }
}

exports.add = async (req, res) => {
    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/user/add", {
                is_layout: is_layout,
                userData: null
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {
    if (req.session.email) {
        let image_name = null;

        if (req.body.iUserId) {
            try {
                if (req.files != null) {
                    if (req.files.vImage.mimetype == "image/png") {
                        var ext = ".png";
                    }
                    if (req.files.vImage.mimetype == "image/jpg") {
                        var ext = ".jpg";
                    }
                    if (req.files.vImage.mimetype == "image/jpeg") {
                        var ext = ".jpeg";
                    }
                    image_name = Date.now() + ext;
                }

                if (req.files != null) {
                    UserModel.findOneAndUpdate({ _id: req.body.iUserId }, { "$set": { "vFirstName": req.body.vFirstName, "vLastName": req.body.vLastName, "vImage": image_name, "vEmail": req.body.vEmail, "eType": req.body.eType, "eStatus": req.body.eStatus } }).exec(function (error, result) {
                        if (!error) {
                            if (req.files != null) {
                                const vImage = req.files.vImage;
                                vImage.mv("./public/assets/upload/user/" + image_name);
                            }
                            res.redirect("/user");
                        } else {
                            console.log(error);
                        }
                    });
                } else {
                    UserModel.findOneAndUpdate({ _id: req.body.iUserId }, { "$set": { "vFirstName": req.body.vFirstName, "vLastName": req.body.vLastName, "vEmail": req.body.vEmail, "eType": req.body.eType, "eStatus": req.body.eStatus } }).exec(function (error, result) {
                        if (!error) {
                            res.redirect("/user");
                        } else {
                            console.log(error);
                        }
                    });
                }
            } catch (error) {
                console.log(error);
            }
        } else {
            if (req.files != null) {
                if (req.files.vImage.mimetype == "image/png") {
                    var ext = ".png";
                }
                if (req.files.vImage.mimetype == "image/jpg") {
                    var ext = ".jpg";
                }
                if (req.files.vImage.mimetype == "image/jpeg") {
                    var ext = ".jpeg";
                }
                image_name = Date.now() + ext;
            }

            const hash = await bcrypt.hash(req.body.vPassword, 10);

            const User = new UserModel({
                vFirstName: req.body.vFirstName,
                vLastName: req.body.vLastName,
                vImage: image_name,
                vEmail: req.body.vEmail,
                vPassword: hash,
                eType: req.body.eType,
                eStatus: req.body.eStatus,
            });

            let email_template = await SystemEmailModel.find();

            try {
                User.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        let criteria = {
                            user_name: result.vFirstName + ' ' + result.vLastName,
                            email_to: result.vEmail,
                            password: req.body.vPassword,
                            email_subject: "eShop Registration",
                            email_template: email_template
                        }
                        EmailLibrary.send_email(criteria);

                        if (req.files != null) {
                            const vImage = req.files.vImage;
                            vImage.mv("./public/assets/upload/user/" + image_name);
                        }

                        res.redirect("/user");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }
    }
};

exports.edit = async (req, res) => {
    var iUserId = req.params.iUserId;
    if (req.session.email) {
        try {
            var userData = await UserModel.findOne({ _id: iUserId });
            is_layout = true;
            res.render("../view/user/add", {
                is_layout: is_layout,
                userData: userData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};
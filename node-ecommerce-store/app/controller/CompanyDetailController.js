const async = require("hbs/lib/async");
const CompanyDetailModel = require("../model/CompanyDetailModel");

exports.add = async(req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/company_detail/add", {
                is_layout: is_layout,
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async(req, res) => {

    if (req.session.email) {
        let favicon_name = null;
        let logo_name = null;

        if (req.body.iCompanyDetailId) {
            try {
                if (req.files != null) {
                    if (req.files.vFavicon.mimetype == "image/png") {
                        var ext = ".png";
                    }
                    if (req.files.vFavicon.mimetype == "image/jpg") {
                        var ext = ".jpg";
                    }
                    if (req.files.vFavicon.mimetype == "image/jpeg") {
                        var ext = ".jpeg";
                    }
                    favicon_name = Date.now() + ext;

                    if (req.files.vLogo.mimetype == "image/png") {
                        var ext = ".png";
                    }
                    if (req.files.vLogo.mimetype == "image/jpg") {
                        var ext = ".jpg";
                    }
                    if (req.files.vLogo.mimetype == "image/jpeg") {
                        var ext = ".jpeg";
                    }
                    logo_name = Date.now() + 1 + ext;
                }

                if (req.files != null) {
                    CompanyDetailModel.findOneAndUpdate({ _id: req.body.iCompanyDetailId }, { "$set": { "tAddress": req.body.tAddress, "tDescription": req.body.tDescription, "vEmail": req.body.vEmail, "vFavicon": favicon_name, "vLogo": logo_name, "vName": req.body.vName, "vNumber": req.body.vNumber, "vWebsite": req.body.vWebsite, "vCopyright": req.body.vCopyright } }).exec(function(error, result) {
                        if (!error) {
                            if (req.files != null) {
                                const vFavicon = req.files.vFavicon;
                                vFavicon.mv("./app/assets/upload/company_detail/" + favicon_name);

                                const vLogo = req.files.vLogo;
                                vLogo.mv("./app/assets/upload/company_detail/" + logo_name);
                            }
                            res.redirect("back");
                        } else {
                            console.log(error);
                        }
                    });
                } else {
                    CompanyDetailModel.findOneAndUpdate({ _id: req.body.iCompanyDetailId }, { "$set": { "tAddress": req.body.tAddress, "tDescription": req.body.tDescription, "vEmail": req.body.vEmail, "vName": req.body.vName, "vNumber": req.body.vNumber, "vWebsite": req.body.vWebsite, "vCopyright": req.body.vCopyright } }).exec(function(error, result) {
                        if (!error) {
                            res.redirect("back");
                        } else {
                            console.log(error);
                        }
                    });
                }
            } catch (error) {
                console.log(error);
            }
        } else {
            if (req.files != null) {
                if (req.files.vFavicon.mimetype == "image/png") {
                    var ext = ".png";
                }
                if (req.files.vFavicon.mimetype == "image/jpg") {
                    var ext = ".jpg";
                }
                if (req.files.vFavicon.mimetype == "image/jpeg") {
                    var ext = ".jpeg";
                }
                favicon_name = Date.now() + ext;

                if (req.files.vLogo.mimetype == "image/png") {
                    var ext = ".png";
                }
                if (req.files.vLogo.mimetype == "image/jpg") {
                    var ext = ".jpg";
                }
                if (req.files.vLogo.mimetype == "image/jpeg") {
                    var ext = ".jpeg";
                }
                logo_name = Date.now() + 1 + ext;
            }
            const Company = new CompanyDetailModel({
                tAddress: req.body.tAddress,
                tDescription: req.body.tDescription,
                vEmail: req.body.vEmail,
                vFavicon: favicon_name,
                vLogo: logo_name,
                vName: req.body.vName,
                vNumber: req.body.vNumber,
                vWebsite: req.body.vWebsite,
                vCopyright: req.body.vCopyright
            });
            try {
                Company.save(function(error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        if (req.files != null) {
                            const vFavicon = req.files.vFavicon;
                            vFavicon.mv("./app/assets/upload/company_detail/" + favicon_name);

                            const vLogo = req.files.vLogo;
                            vLogo.mv("./app/assets/upload/company_detail/" + logo_name);
                        }
                        res.redirect("/add");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }

    }
};

exports.edit = async(req, res) => {

    var iCompanyDetailId = req.params.iCompanyDetailId;



    if (req.session.email) {
        try {
            var companyData = await CompanyDetailModel.findOne({ _id: iCompanyDetailId });
            is_layout = true;
            res.render("../view/company_detail/add", {
                is_layout: is_layout,
                companyData: companyData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};
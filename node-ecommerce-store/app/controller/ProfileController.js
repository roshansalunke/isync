const async = require("hbs/lib/async");
const UserModel = require("../model/UserModel");
const bcrypt = require("bcrypt");

exports.add_action = async (req, res) => {

    if (req.session.email) {
        if (req.body.iUserId) {
            try {
                let image_name = null;

                if (req.files != null) {
                    if (req.files.vImage.mimetype == "image/png") {
                        var ext = ".png";
                    }
                    if (req.files.vImage.mimetype == "image/jpg") {
                        var ext = ".jpg";
                    }
                    if (req.files.vImage.mimetype == "image/jpeg") {
                        var ext = ".jpeg";
                    }
                    image_name = Date.now() + ext;
                }

                if (req.files != null) {
                    UserModel.findOneAndUpdate({ _id: req.body.iUserId }, {
                        "$set": {
                            "vFirstName": req.body.vFirstName,
                            "vLastName": req.body.vLastName,
                            "vImage": image_name,
                            "vEmail": req.body.vEmail,
                            "vPassword": req.body.vPassword,
                            "eType": req.body.eType,
                            "eStatus": req.body.eStatus
                        }
                    }).exec(function (error, result) {
                        if (!error) {
                            if (req.files != null) {
                                const vImage = req.files.vImage;
                                // /var/www/html/projects/nodejs/node-ecommerce-store/assets/upload/user
                                // ./app/assets/upload/user/
                                vImage.mv("./public/assets/upload/user/" + image_name);
                            }
                            res.redirect("/user");
                        } else {
                            console.log(error);
                        }
                    });
                } else {
                    UserModel.findOneAndUpdate({ _id: req.body.iUserId }, {
                        "$set": {
                            "vFirstName": req.body.vFirstName,
                            "vLastName": req.body.vLastName,
                            "vEmail": req.body.vEmail,
                            "vPassword": req.body.vPassword,
                            "eType": req.body.eType,
                            "eStatus": req.body.eStatus
                        }
                    }).exec(function (error, result) {
                        if (!error) {
                            res.redirect("/user");
                        } else {
                            console.log(error);
                        }
                    });
                }
            } catch (error) {
                console.log(error);
            }
        }
    }
};

exports.edit = async (req, res) => {

    var iUserId = req.params.iUserId;

    if (req.session.email) {
        try {
            var userData = await UserModel.findOne({ _id: iUserId });
            is_layout = true;
            res.render("../view/profile/add", {
                is_layout: is_layout,
                userData: userData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};

exports.changePassword = async (req, res) => {

    var iUserId = req.params.iUserId;

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/profile/changePassword", {
                iUserId: iUserId,
                is_layout: is_layout
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};

exports.changePasswordAction = async (req, res) => {

    if (req.session.email) {
        if (req.body.iUserId) {
            try {
                const hash = await bcrypt.hash(req.body.vPassword, 10);

                UserModel.findOneAndUpdate({ _id: req.body.iUserId }, { "$set": { "vPassword": hash } }).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/dashboard");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        }
    }

};
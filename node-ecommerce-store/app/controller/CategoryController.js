const async = require("hbs/lib/async");
const CategoryModel = require("../model/CategoryModel");
const Paginator = require("../library/PaginatorLibrary");

exports.index = async (req, res) => {

    if (req.session.email) {
        is_layout = true;
        res.render("../view/category/index", {
            is_layout: is_layout,
        });
    } else {
        res.redirect("/");
    }

};

exports.ajax_category = async (req, res) => {
    var vCategory = req.body.vCategory;
    var eStatus = req.body.eStatus;
    var eFeature = req.body.eFeature;
    var iCategoryId = req.body.iCategoryId;
    var vAction = req.body.vAction;
    var vPage = req.body.vPage;

    if (vAction === "delete" && iCategoryId != null) {
        await CategoryModel.deleteOne({ _id: iCategoryId });
    }

    if (vAction === "multiple_delete" && iCategoryId != null) {
        let categoryID = iCategoryId.split(',');

        await CategoryModel.deleteMany({ _id: { $in: categoryID } });
    }

    if (vAction === "search") {
        var SQL = {};
        if (vCategory.length > 0) {
            var vCategorySearch = new RegExp(vCategory, "i");
            SQL.vCategory = vCategorySearch;
        }
        if (eStatus.length > 0) {
            var eStatusSearch = eStatus;
            SQL.eStatus = eStatusSearch;
        }
        if (eFeature.length > 0) {
            var eFeatureSearch = eFeature;
            SQL.eFeature = eFeatureSearch;
        }

        try {
            // Pagination
            var dataCount = await CategoryModel.count(SQL);
            let vPage = 1;
            let vItemPerPage = 10;
            let vCount = dataCount;

            if (req.body.vPage != "") {
                vPage = req.body.vPage;
            }

            let criteria = {
                vPage: vPage,
                vItemPerPage: vItemPerPage,
                vCount: vCount
            }

            let paginator = Paginator.pagination(criteria);

            let start = (vPage - 1) * vItemPerPage;
            let limit = vItemPerPage;
            // End

            var categoryData = await CategoryModel.find(SQL).skip(start).limit(limit);

            res.render("../view/category/ajax_category", {
                layout: false,
                categoryData: categoryData,
                paginator: paginator
            });
        } catch (error) {
            res.render("../view/category/ajax_category", {
                layout: false,
                categoryData: error
            });
        }
    } else {
        try {
            // Pagination
            var dataCount = await CategoryModel.count(SQL);
            let vPage = 1;
            let vItemPerPage = 10;
            let vCount = dataCount;

            if (req.body.vPage != "") {
                vPage = req.body.vPage;
            }

            let criteria = {
                vPage: vPage,
                vItemPerPage: vItemPerPage,
                vCount: vCount
            }

            let paginator = Paginator.pagination(criteria);

            let start = (vPage - 1) * vItemPerPage;
            let limit = vItemPerPage;
            // End

            var categoryData = await CategoryModel.find(SQL).skip(start).limit(limit);

            res.render("../view/category/ajax_category", {
                layout: false,
                categoryData: categoryData,
                paginator: paginator
            });
        } catch (error) {
            res.render("../view/category/ajax_category", {
                layout: false,
                categoryData: error
            });
        }
    }
}

exports.add = async (req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/category/add", {
                is_layout: is_layout,
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {

    if (req.session.email) {
        var image_name = null;

        if (req.body.iCategoryId) {
            try {
                if (req.files != null) {
                    if (req.files.vImage.mimetype == "image/png") {
                        var ext = ".png";
                    }
                    if (req.files.vImage.mimetype == "image/jpg") {
                        var ext = ".jpg";
                    }
                    if (req.files.vImage.mimetype == "image/jpeg") {
                        var ext = ".jpeg";
                    }
                    image_name = Date.now() + ext;
                }
                if (req.files != null) {
                    CategoryModel.findOneAndUpdate({ _id: req.body.iCategoryId }, { "$set": { "vCategory": req.body.vCategory, "eStatus": req.body.eStatus, "vImage": image_name, "eFeature": req.body.eFeature, "iOrder": req.body.iOrder } }).exec(function (error, result) {
                        if (!error) {
                            if (req.files != null) {
                                const vImage = req.files.vImage;
                                vImage.mv("./public/assets/upload/category/" + image_name);
                            }
                            res.redirect("/category");
                        } else {
                            console.log(error);
                        }
                    });
                } else {
                    CategoryModel.findOneAndUpdate({ _id: req.body.iCategoryId }, { "$set": { "vCategory": req.body.vCategory, "eStatus": req.body.eStatus, "eFeature": req.body.eFeature, "iOrder": req.body.iOrder } }).exec(function (error, result) {
                        if (!error) {
                            res.redirect("/category");
                        } else {
                            console.log(error);
                        }
                    });
                }
            } catch (error) {
                console.log(error);
            }
        } else {
            if (req.files != null) {
                if (req.files.vImage.mimetype == "image/png") {
                    var ext = ".png";
                }
                if (req.files.vImage.mimetype == "image/jpg") {
                    var ext = ".jpg";
                }
                if (req.files.vImage.mimetype == "image/jpeg") {
                    var ext = ".jpeg";
                }
                image_name = Date.now() + ext;
            }
            const Category = new CategoryModel({
                vCategory: req.body.vCategory,
                eStatus: req.body.eStatus,
                eFeature: req.body.eFeature,
                iOrder: req.body.iOrder,
                vImage: image_name,
            });
            try {
                Category.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        if (req.files != null) {
                            const vImage = req.files.vImage;
                            vImage.mv("./public/assets/upload/category/" + image_name);
                        }
                        res.redirect("/category");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }

    }
};

exports.edit = async (req, res) => {

    var iCategoryId = req.params.iCategoryId;

    if (req.session.email) {
        try {
            var categoryData = await CategoryModel.findOne({ _id: iCategoryId });
            is_layout = true;
            res.render("../view/category/add", {
                is_layout: is_layout,
                categoryData: categoryData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};
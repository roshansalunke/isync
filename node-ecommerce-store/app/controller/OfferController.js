const async = require("hbs/lib/async");
const OfferModel = require("../model/OfferModel");
const Paginator = require("../library/PaginatorLibrary");

exports.index = async(req, res) => {

    if (req.session.email) {
        is_layout = true;
        res.render("../view/offer/index", {
            is_layout: is_layout,
        });
    } else {
        res.redirect("/");
    }

};

exports.ajax_listing = async(req, res) => {
    var iOfferId = req.body.iOfferId;
    var vOffer = req.body.vOffer;
    var eType = req.body.eType;
    var vValue = req.body.vValue;
    var eStatus = req.body.eStatus;
    var eFeature = req.body.eFeature;
    var vAction = req.body.vAction;
    var vPage = req.body.vPage;

    if (vAction === "delete" && iOfferId != null) {
        await OfferModel.deleteOne({ _id: iOfferId });
    }

    if (vAction === "search") {
        var SQL = {};
        if (vOffer.length > 0) {
            var vOfferSearch = new RegExp(vOffer, "i");
            SQL.vOffer = vOfferSearch;
        }
        if (eStatus.length > 0) {
            var eStatusSearch = eStatus;
            SQL.eStatus = eStatusSearch;
        }
        if (eFeature.length > 0) {
            var eFeatureSearch = eFeature;
            SQL.eFeature = eFeatureSearch;
        }
        if (eType.length > 0) {
            var eTypeSearch = eType;
            SQL.eType = eTypeSearch;
        }
        if (vValue.length > 0) {
            var vValueSearch = vValue;
            SQL.vValue = vValueSearch;
        }

        try {
            var dataCount = await OfferModel.count(SQL);

            // Pagination
            let vPage = 1;
            let vItemPerPage = 10;
            let vCount = dataCount;

            if (req.body.vPage != "") {
                vPage = req.body.vPage;
            }

            let criteria = {
                vPage: vPage,
                vItemPerPage: vItemPerPage,
                vCount: vCount
            }

            let paginator = Paginator.pagination(criteria);

            let start = (vPage - 1) * vItemPerPage;
            let limit = vItemPerPage;
            // End

            var offerData = await OfferModel.find(SQL).skip(start).limit(limit);

            res.render("../view/offer/ajax_listing", {
                layout: false,
                offerData: offerData,
                paginator: paginator,
                datalimit: limit,
                dataCount: dataCount
            });
        } catch (error) {
            res.render("../view/offer/ajax_listing", {
                layout: false,
                offerData: error
            });
        }
    } else {
        try {
            var offerData = await OfferModel.find();
            res.render("../view/offer/ajax_listing", {
                layout: false,
                offerData: offerData
            });
        } catch (error) {
            res.render("../view/offer/ajax_listing", {
                layout: false,
                offerData: error
            });
        }
    }
}

exports.add = async(req, res) => {

    if (req.session.email) {
        try {
            is_layout = true;
            res.render("../view/offer/add", {
                is_layout: is_layout,
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async(req, res) => {

    if (req.session.email) {

        if (req.body.iOfferId) {
            try {
                OfferModel.findOneAndUpdate({ _id: req.body.iOfferId }, {
                    "$set": {
                        "vOffer": req.body.vOffer,
                        "eType": req.body.eType,
                        "vValue": req.body.vValue,
                        "eStatus": req.body.eStatus,
                        "eFeature": req.body.eFeature
                    }
                }).exec(function(error, result) {
                    if (!error) {
                        res.redirect("/offer");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            const Offer = new OfferModel({
                vOffer: req.body.vOffer,
                eType: req.body.eType,
                vValue: req.body.vValue,
                eStatus: req.body.eStatus,
                eFeature: req.body.eFeature,
            });
            try {
                Offer.save(function(error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        res.redirect("/offer");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }

    }
};

exports.edit = async(req, res) => {

    var iOfferId = req.params.iOfferId;

    if (req.session.email) {
        try {
            var offerData = await OfferModel.findOne({ _id: iOfferId });
            is_layout = true;
            res.render("../view/offer/add", {
                is_layout: is_layout,
                offerData: offerData
            });
        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }

};
const async = require("hbs/lib/async");
const ProductModel = require("../model/ProductModel");
const CategoryModel = require("../model/CategoryModel");
const Paginator = require("../library/PaginatorLibrary");
const OfferModel = require("../model/OfferModel");

exports.index = async (req, res) => {

    if (req.session.email) {
        var categoryData = await CategoryModel.find().exec();
        var offerData = await OfferModel.find().exec();

        is_layout = true;
        res.render("../view/product/index", {
            is_layout: is_layout,
            categoryData: categoryData,
            offerData: offerData
        });
    } else {
        res.redirect("/");
    }

};

exports.ajax_product = async (req, res) => {
    var iCategoryId = req.body.iCategoryId;
    var vProduct = req.body.vProduct;
    var iPrice = req.body.iPrice;
    var eStatus = req.body.eStatus;
    var eFeature = req.body.eFeature;
    var iProductId = req.body.iProductId;
    var iOfferId = req.body.iOfferId;
    var vAction = req.body.vAction;

    if (vAction === "delete" && iProductId != null) {
        await ProductModel.deleteOne({ _id: iProductId });
    }

    if (vAction === "multiple_delete" && iProductId != null) {
        let productID = iProductId.split(',');

        await ProductModel.deleteMany({ _id: { $in: productID } });
    }

    if (vAction === "search") {
        var SQL = {};
        if (iCategoryId.length > 0) {
            var iCategoryIdSearch = iCategoryId;
            SQL.iCategoryId = iCategoryIdSearch;
        }
        if (vProduct.length > 0) {
            var vProductSearch = new RegExp(vProduct, "i");
            SQL.vProduct = vProductSearch;
        }
        if (iPrice.length > 0) {
            var iPriceSearch = iPrice;
            SQL.iPrice = iPriceSearch;
        }
        if (eStatus.length > 0) {
            var eStatusSearch = eStatus;
            SQL.eStatus = eStatusSearch;
        }
        if (eFeature.length > 0) {
            var eFeatureSearch = eFeature;
            SQL.eFeature = eFeatureSearch;
        }
        if (iOfferId.length > 0) {
            var iOfferIdSearch = iOfferId;
            SQL.iOfferId = iOfferIdSearch;
        }

        try {
            // Pagination
            var productDataCount = await ProductModel.count(SQL);
            let vPage = 1;
            let vItemPerPage = 10;
            let vCount = productDataCount;

            if (req.body.vPage != "") {
                vPage = req.body.vPage;
            }

            let criteria = {
                vPage: vPage,
                vItemPerPage: vItemPerPage,
                vCount: vCount
            }

            let paginator = Paginator.pagination(criteria);

            let start = (vPage - 1) * vItemPerPage;
            let limit = vItemPerPage;
            // End

            var productData = await ProductModel.find(SQL).skip(start).limit(limit).sort({ _id: 'desc' }).populate({
                path: 'iCategoryId',
                select: 'vCategory'
            }).populate({
                path: 'iOfferId',
                select: 'vOffer'
            });

            res.render("../view/product/ajax_product", {
                layout: false,
                productData: productData,
                paginator: paginator
            });
        } catch (error) {
            res.render("../view/product/ajax_product", {
                layout: false,
                productData: error
            });
        }
    } else {
        try {
            // Pagination
            var productDataCount = await ProductModel.count(SQL);
            let vPage = 1;
            let vItemPerPage = 10;
            let vCount = productDataCount;

            if (req.body.vPage != "") {
                vPage = req.body.vPage;
            }

            let criteria = {
                vPage: vPage,
                vItemPerPage: vItemPerPage,
                vCount: vCount
            }

            let paginator = Paginator.pagination(criteria);

            let start = (vPage - 1) * vItemPerPage;
            let limit = vItemPerPage;
            // End

            var productData = await ProductModel.find(SQL).skip(start).limit(limit).sort({ _id: 'desc' }).populate({
                path: 'iCategoryId',
                select: 'vCategory'
            }).populate({
                path: 'iOfferId',
                select: 'vOffer'
            });

            // var productData = await ProductModel.find().lean().exec();

            // for (let index = 0; index < productData.length; index++) {

            //     let iCategoryId = productData[index].iCategoryId;

            //     var categoryData = await CategoryModel.findOne({_id:iCategoryId}).exec();

            //     productData[index].vCategory = categoryData.vCategory;

            // }

            res.render("../view/product/ajax_product", {
                layout: false,
                productData: productData,
                paginator: paginator
            });
        } catch (error) {
            res.render("../view/product/ajax_product", {
                layout: false,
                productData: error
            });
        }
    }
}

exports.add = async (req, res) => {

    if (req.session.email) {
        try {
            var categoryData = await CategoryModel.find();

            is_layout = true;
            res.render("../view/product/add", {
                is_layout: is_layout,
                categoryData: categoryData,
                productData: null
            });

        } catch (error) {
            console.log(error);
        }
    } else {
        res.redirect("/");
    }
};

exports.add_action = async (req, res) => {

    if (req.session.email) {

        let image_name = null;

        if (req.body.iProductId) {
            try {
                if (req.files != null) {
                    if (req.files != null) {
                        if (req.files.vImage.mimetype == "image/png") {
                            var ext = ".png";
                        }
                        if (req.files.vImage.mimetype == "image/jpg") {
                            var ext = ".jpg";
                        }
                        if (req.files.vImage.mimetype == "image/jpeg") {
                            var ext = ".jpeg";
                        }
                        image_name = Date.now() + ext;
                    }

                    ProductModel.findOneAndUpdate({ _id: req.body.iProductId }, { "$set": { "iCategoryId": req.body.iCategoryId, "vProduct": req.body.vProduct, "vImage": image_name, "iPrice": req.body.iPrice, "eStatus": req.body.eStatus, "eFeature": req.body.eFeature } }).exec(function (error, result) {
                        if (!error) {
                            if (req.files != null) {
                                const vImage = req.files.vImage;
                                vImage.mv("./public/assets/upload/product/" + image_name);
                            }
                            res.redirect("/product");
                        } else {
                            console.log(error);
                        }
                    });
                } else {
                    ProductModel.findOneAndUpdate({ _id: req.body.iProductId }, { "$set": { "iCategoryId": req.body.iCategoryId, "vProduct": req.body.vProduct, "iPrice": req.body.iPrice, "eStatus": req.body.eStatus, "eFeature": req.body.eFeature } }).exec(function (error, result) {
                        if (!error) {
                            res.redirect("/product");
                        } else {
                            console.log(error);
                        }
                    });
                }
            } catch (error) {
                console.log(error);
            }
        } else {
            if (req.files != null) {
                if (req.files.vImage.mimetype == "image/png") {
                    var ext = ".png";
                }
                if (req.files.vImage.mimetype == "image/jpg") {
                    var ext = ".jpg";
                }
                if (req.files.vImage.mimetype == "image/jpeg") {
                    var ext = ".jpeg";
                }
                image_name = Date.now() + ext;
            }
            const Product = new ProductModel({
                iCategoryId: req.body.iCategoryId,
                vProduct: req.body.vProduct,
                vImage: image_name,
                iPrice: req.body.iPrice,
                eStatus: req.body.eStatus,
                eFeature: req.body.eFeature,
            });
            try {
                Product.save(function (error, result) {
                    if (error) {
                        console.log(error);
                    } else {
                        if (req.files != null) {
                            const vImage = req.files.vImage;
                            vImage.mv("./public/assets/upload/product/" + image_name);
                        }
                        res.redirect("/product");
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }

    }
};

exports.edit = async (req, res) => {

    var iProductId = req.params.iProductId;

    if (req.session.email) {
        try {
            var productData = await ProductModel.findOne({ _id: iProductId });
            var categoryData = await CategoryModel.find();

            if (productData != null) {
                is_layout = true;
                res.render("../view/product/add", {
                    is_layout: is_layout,
                    productData: productData,
                    categoryData: categoryData
                });
            } else {
                res.redirect("/product");
            }
        } catch (error) {
            console.log(error);
            res.redirect("/product");
        }
    } else {
        res.redirect("/");
    }

};

exports.offer = async (req, res) => {

    var iProductId = req.params.iProductId;

    if (req.session.email) {
        try {
            var productData = await ProductModel.findOne({ _id: iProductId });
            var offerData = await OfferModel.find();

            if (productData != null) {
                is_layout = true;
                res.render("../view/product/offer", {
                    is_layout: is_layout,
                    productData: productData,
                    offerData: offerData
                });
            } else {
                res.redirect("/product");
            }
        } catch (error) {
            console.log(error);
            res.redirect("/product");
        }
    } else {
        res.redirect("/");
    }

};

exports.add_offer_action = async (req, res) => {

    if (req.session.email) {

        if (req.body.iProductId) {
            try {
                ProductModel.findOneAndUpdate({ _id: req.body.iProductId }, { "$set": { "iOfferId": req.body.iOfferId } }).exec(function (error, result) {
                    if (!error) {
                        res.redirect("/product");
                    } else {
                        console.log(error);
                    }
                });
            } catch (error) {
                console.log(error);
            }
        } else {
            console.log("Id not found");
            res.redirect("/product");
        }

    }
};
const { rejects } = require("assert");
const { Handlebars } = require("express-handlebars");
const req = require("express/lib/request");
const async = require("hbs/lib/async");
const { resolve } = require("path");
const CompanyDetailModel = require("../model/CompanyDetailModel");

module.exports = {
    if_equal: function (a, b, options) {
        if (a == b) {
            return options.fn(this);
        }
        return options.inverse(this);
    },
    bar: function (req, res, next) {
        return "BAR!";
    },
    isSelected: function (value, key) {
        return value == key ? "selected" : "";
    },
    contains: (Needle, Haystack, options) => {
        Needle = Haystack;
        Haystack = Haystack;
        return (Haystack.indexOf(Needle) > -1) ? options.fn(this) : options.inverse(this);
    },
    current_class: function () {
        return req.route;
    },
    if_condition: function (value1, value2, options) {
        if (value1 === value2) {
            return options.fn(this);
        }
        return options.inverse(this);
    },
    if_count: function (value, option) {
        if (value.length > 0) {
            return option.fn(this);
        }
        return option.inverse(this);
    },
    substr: function (str, length) {
        var maxLength = length;
        if (str.length > maxLength) {
            return str.substr(0, maxLength) + '...';
        } else {
            return str;
        }
    }
};
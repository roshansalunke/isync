const mongoose = require("mongoose");

const ProductSchema = mongoose.Schema({
    iCategoryId: {
        type: String,
        required: true,
        ref: 'category',
    },
    vProduct: {
        type: String,
        required: true,
    },
    vImage: {
        type: String,
    },
    iPrice: {
        type: Number,
        required: true,
    },
    iOfferId: {
        type: String,
        ref: 'offer',
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
        required: true,
    },
    eFeature: {
        type: String,
        enum: ['Yes', 'No'],
        default: 'No',
        required: true,
    }
});

module.exports = mongoose.model("product", ProductSchema);
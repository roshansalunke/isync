const mongoose = require("mongoose");

const CategorySchema = mongoose.Schema({
    vCategory: {
        type: String,
        required: true,
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
        required: true,
    },
    eFeature: {
        type: String,
        enum: ['Yes', 'No'],
        default: 'No',
        required: true,
    },
    iOrder: {
        type: Number,
        required: true,
    },
    vImage: {
        type: String,
    },
});

module.exports = mongoose.model("category", CategorySchema);
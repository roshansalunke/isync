const mongoose = require("mongoose");

const OfferSchema = mongoose.Schema({
    vOffer: {
        type: String,
        required: true,
    },
    eType: {
        type: String,
        enum: ['Fixed', 'Percentage'],
        required: true,
    },
    vValue: {
        type: String,
        required: true,
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
        required: true,
    },
    eFeature: {
        type: String,
        enum: ['Yes', 'No'],
        default: 'No',
        required: true,
    }
});

module.exports = mongoose.model("offer", OfferSchema);
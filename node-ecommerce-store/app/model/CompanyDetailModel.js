const mongoose = require("mongoose");

const CompanyDetailSchema = mongoose.Schema({
    tAddress: {
        type: String,
    },
    tDescription: {
        type: String,
    },
    vEmail: {
        type: String,
    },
    vFavicon: {
        type: String,
    },
    vLogo: {
        type: String
    },
    vName: {
        type: String
    },
    vNumber: {
        type: String
    },
    vWebsite: {
        type: String
    },
    vCopyright: {
        type: String
    },
});

module.exports = mongoose.model("company_detail", CompanyDetailSchema);
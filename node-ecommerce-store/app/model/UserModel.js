const mongoose = require("mongoose");

const UserSchema = mongoose.Schema({
    vFirstName: {
        type: String,
        required: true,
    },
    vLastName: {
        type: String,
        required: true,
    },
    vImage: {
        type: String,
    },
    vEmail: {
        type: String,
        required: true,
    },
    vPassword: {
        type: String,
        required: true,
    },
    eType: {
        type: String,
        enum: ['Admin', 'User'],
        default: 'User',
        required: true,
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
        required: true,
    },
});

module.exports = mongoose.model("user", UserSchema);
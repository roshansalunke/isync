const mongoose = require("mongoose");

const LanguageLabelSchema = mongoose.Schema({
    vLabel: {
        type: String,
        required: true,
    },
    vTitle: {
        type: String,
        required: true,
    },
    eStatus: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active',
        required: true,
    }
});

module.exports = mongoose.model("language_label", LanguageLabelSchema);
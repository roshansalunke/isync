const express = require("express");
const router = express.Router();
const SystemEmailController = require("../controller/SystemEmailController");

router.get("/", SystemEmailController.index);
router.get("/add", SystemEmailController.add);
router.post("/add_action", SystemEmailController.add_action);
router.get("/edit/:iSystemEmailId", SystemEmailController.edit);
router.post("/ajax_listing", SystemEmailController.ajax_listing);

module.exports = router;

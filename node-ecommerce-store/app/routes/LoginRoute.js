const express = require("express");
const router = express.Router();
const LoginController = require("../controller/LoginController");

router.get("/", LoginController.index);
router.post("/login_action", LoginController.login_action);
router.get("/logout", LoginController.logout);

module.exports = router;

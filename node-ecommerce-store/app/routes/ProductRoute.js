const express = require("express");
const router = express.Router();
const ProductController = require("../controller/ProductController");

router.get("/", ProductController.index);
router.get("/add", ProductController.add);
router.post("/add_action", ProductController.add_action);
router.get("/edit/:iProductId", ProductController.edit);
router.post("/ajax_product", ProductController.ajax_product);
router.get("/offer/:iProductId", ProductController.offer);
router.post("/offer/add_offer_action", ProductController.add_offer_action);

module.exports = router;
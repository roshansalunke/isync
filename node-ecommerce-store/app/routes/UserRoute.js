const express = require("express");
const router = express.Router();
const UserController = require("../controller/UserController");

router.get("/", UserController.index);
router.get("/add", UserController.add);
router.post("/add_action", UserController.add_action);
router.get("/edit/:iUserId", UserController.edit);
router.post("/ajax_user", UserController.ajax_user);

module.exports = router;

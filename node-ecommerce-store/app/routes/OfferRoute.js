const express = require("express");
const router = express.Router();
const OfferController = require("../controller/OfferController");

router.get("/", OfferController.index);
router.get("/add", OfferController.add);
router.post("/add_action", OfferController.add_action);
router.get("/edit/:iOfferId", OfferController.edit);
router.post("/ajax_listing", OfferController.ajax_listing);

module.exports = router;
const express = require("express");
const router = express.Router();
const LanguageLabelController = require("../controller/LanguageLabelController");

router.get("/", LanguageLabelController.index);
router.get("/add", LanguageLabelController.add);
router.post("/add_action", LanguageLabelController.add_action);
router.get("/edit/:iLanguageLabelId", LanguageLabelController.edit);
router.post("/ajax_listing", LanguageLabelController.ajax_listing);
router.get("/generate", LanguageLabelController.generate);

module.exports = router;

const express = require("express");
const router = express.Router();
const CategoryController = require("../controller/CategoryController");

router.get("/", CategoryController.index);
router.get("/add", CategoryController.add);
router.post("/add_action", CategoryController.add_action);
router.get("/edit/:iCategoryId", CategoryController.edit);
router.post("/ajax_category", CategoryController.ajax_category);

module.exports = router;

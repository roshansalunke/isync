module.exports = app => {
    const LoginRoute = require("./app/routes/LoginRoute")(app);
    const DashboardRoute = require("./app/routes/DashboardRoute")(app);
    const CategoryRoute = require("./app/routes/CategoryRoute")(app);
    const UserRoute = require("./app/routes/UserRoute")(app);
    const ProductRoute = require("./app/routes/ProductRoute")(app);
}
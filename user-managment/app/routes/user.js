const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');


//create, find, update, delete
router.get('/',userController.view);
router.post('/',userController.find);
router.get('/edit/:id',userController.edit);
router.post('/edit_action',userController.edit_action);
router.get('/add',userController.add);
router.post('/add_action',userController.add_action);
router.get('/delete/:id',userController.delete);



//Router
// router.get('', (req, res)=>{
//     res.render('home');
// });

module.exports = router;

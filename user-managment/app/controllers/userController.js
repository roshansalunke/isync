const mysql = require('mysql2');

//Connection Pool
const pool = mysql.createPool({

    connectionLimit: 100,
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME
});


//view User
exports.view = (req, res) => {
    pool.getConnection((err, connection) => {
        if (err) throw err; //not connected!
        console.log('Connected as ID ' + connection.threadId);
        //user connection
        connection.query('select * from user', (err, rows) => {
            //Whene done with the connection, release it
            connection.release();
            if (!err) {
                res.render('home', { rows });
            } else {
                console.log(err);
            }
            console.log('The data from user table: \n', rows);
        })
    });
}

//find user by search
exports.find = (req, res) => {
    pool.getConnection((err, connection) => {
        if (err) throw err; //not connected!
        console.log('Connected as ID ' + connection.threadId);

        let searchTearm = req.body.search;

        //user connection
        var sql = 'select * from user where first_name like ? OR last_name like ? OR email like ? OR phone like ?';
        connection.query(sql, ['%' + searchTearm + '%', '%' + searchTearm + '%', '%' + searchTearm + '%', '%' + searchTearm + '%'], (err, rows) => {
            //Whene done with the connection, release it
            connection.release();
            if (!err) {
                res.render('home', { rows });
            } else {
                console.log(err);
            }
            console.log('The data from user table: \n', rows);
        })
    });
}

//edit User
exports.edit = (req, res) => {
    pool.getConnection((err, connection) => {
        if (err) throw err; //not connected!
        console.log('Connected as ID ' + connection.threadId);
        //user connection

        let id = req.params.id;

        var sql = `select * from user where id = ${id}`;

        connection.query(sql, (err, rows) => {
            //Whene done with the connection, release it
            connection.release();
            if (!err) {
                res.render('edit', { rows });
            } else {
                console.log(err);
            }
            console.log('The data from user table: \n', rows);
        })
    });
}

//edit User
exports.edit_action = (req, res) => {
    pool.getConnection((err, connection) => {
        if (err) throw err; //not connected!
        console.log('Connected as ID ' + connection.threadId);
        //user connection

        let id = req.body.id;
        let first_name = req.body.first_name;
        let last_name = req.body.last_name;
        let email = req.body.email;
        let phone = req.body.phone_no;
        console.log(`Dqdddawcawcwdadcadc  ${id} sdjchgqlhvblchbvw`);

        var sql = `UPDATE user SET first_name='${first_name}', last_name='${last_name}', email='${email}', phone=${phone} where id = ${id}`;

        connection.query(sql, (err, rows) => {
            //Whene done with the connection, release it
            connection.release();
            if (!err) {
                res.redirect('/');
            } else {
                console.log(err);
            }
            console.log('The data from user table: \n', rows);
        })
    });
}

//add User
exports.add = (req, res) => {
    res.render('add');
    
}



//add User action
exports.add_action = (req, res) => {
    pool.getConnection((err, connection) => {
        if (err) throw err; //not connected!
        console.log('Connected as ID ' + connection.threadId);
        //user connection

        // let id = req.body.id;
        let first_name = req.body.first_name;
        let last_name = req.body.last_name;
        let email = req.body.email;
        let phone = req.body.phone_no;
        let comments = req.body.comments;
        // console.log(`Dqdddawcawcwdadcadc  ${id} sdjchgqlhvblchbvw`);

        var sql = `INSERT INTO user (id, first_name, last_name, email, phone, comments) VALUES (NULL, '${first_name}','${last_name}','${email}', '${phone}', '${comments}')`;

        connection.query(sql, (err, rows) => {
            //Whene done with the connection, release it
            connection.release();
            if (!err) {
                res.redirect('/');
            } else {
                console.log(err);
            }
            console.log('The data from user table: \n', rows);
        })
    });
}


//edit User
exports.delete = (req, res) => {
    pool.getConnection((err, connection) => {
        if (err) throw err; //not connected!
        console.log('Connected as ID ' + connection.threadId);
        //user connection

        let id = req.params.id;

        var sql = `DELETE FROM user where id = ${id}`;

        connection.query(sql, (err, rows) => {
            //Whene done with the connection, release it
            connection.release();
            if (!err) {
                res.redirect('/');
            } else {
                console.log(err);
            }
            console.log('The data from user table: \n', rows);
        })
    });
}